import { View, StyleSheet } from 'react-native';
import CardTinder from '../components/game/CardTinder';

const GameScreen = ({navigation} : {navigation: any}) => {
  return (
    <View style={styles.container}>
      <CardTinder />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});


export default GameScreen;