import { View, Text, Button, StyleSheet } from 'react-native';

const MainMenuScreen = ({navigation} : {navigation: any}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Tab Two bonjour</Text>
      <Button
          title='Push Game Screen'
          color='#710ce3'
          onPress={() => navigation.navigate('Game')} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});


export default MainMenuScreen;