import React from "react";
import {
    Center,
    Heading,
    VStack,
} from "native-base";

import { View, Text, Button, StyleSheet } from 'react-native';
import NativeBaseIcon from "../components/NativeBaseIcon";

const WelcomeScreen = ({navigation} : {navigation: any}) => {
    return (
        <View style={styles.root}>
            <Center
                _dark={{ bg: "blueGray.900" }}
                _light={{ bg: "blueGray.50" }}
                px={4}
                flex={1}
            >
                <VStack space={5} alignItems="center">
                    <NativeBaseIcon />
                    <Heading size="lg">Welcome to NED : We have the POWER !</Heading>
                    <Button
                        title='Push MainMenu Screen'
                        color='#710ce3'
                        onPress={() => navigation.navigate('MainMenu')} />
                </VStack>
            </Center>

        </View>
    );

};

export default WelcomeScreen;


const styles = StyleSheet.create({
    root: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'whitesmoke'
    }
});